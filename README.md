# TouchDroid

- TouchDroid is an Android Application to Emulate input devices i.e. Touchpad and Keyboard to control Windows computer via local Network. [TouchServer](https://www.github.com/Akshayaap/TouchServer) is reuired to be Running on Windows Operating System in order to capture data sent by Application from network and to register Appropriate events to Host Operating System.
- An older version of the Application is available via [This Repo](https://apt.izzysoft.de/fdroid/index/apk/com.akshayaap.touchdroid) and [F-Droid](https://f-droid.org/packages/com.akshayaap.mouseremote) repo.
  
  ## Table of Content
  - [Technologies](#technologies)
  - [Setup](#setup)
  - [Working](#working)
  - [Screenshots](#screenshots)
  - [Contributors](#contributors)
  
  ## Technologies
  ### For Desktop Server Application
  - C++.
  - C#.

  ### For Android Application
  - Java v17.
  - Android Studio.
  
  ## Setup
  ### For Users
  - Download the zip file from the [TouchServer](https://www.github.com/Akshayaap/TouchServer).
  - Install android [TouchDroid](https://wwww.github.com/Akshayaap/TouchDroid) apk.
  - Extract MouseRemoteServer.zip at your favorite location in your Windows PC.
  - Run `UI.exe`.
  - Now you will be able to control Server.
  - Open MouseRemote application in android.
  - The app will connect to server automatically.
  - There will be touch area, Left and Right Buttons and Wheel.
  <br/><br/>
  Note: **The mobile and server must be in same network**
  For More See: [Documentation](https://www.github.com/Akshayaap/Documentation).
  
  ## Working
  How the program works:-
  - Press `Start` from Desktop-GUI to start.
  - Press `Stop` from Desktop-GUI to stop.
  - You can then Easily control your Computer by using your phone as touchpad.
  
  ## Screenshots
  Some screenshots of the program:-
  - ***Under progress***
  
  ## Contributors
  - @Akshayaap- [Akshay Parmar](https://github.com/Akshayaap).
  - @SKR301- [Saurav Kumar](https://github.com/SKR301).
